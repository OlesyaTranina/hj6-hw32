var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

var  students = []; 

var commonShow =function () {
	console.log('Студент %s набрал %d баллов', this.name, this.point);
}
//наполняем массив объектами
for (var i=0, imax = studentsAndPoints.length-1;i < imax; i+=2){
	students.push(
		{	name:  studentsAndPoints[i],
			point: studentsAndPoints[i+1],
			show: commonShow 
		});
};
//добавляем новых студентов
students.push(
	{	name: 'Николай Фролов',
	point: 0,
	show: commonShow 
},
{	name:  'Олег Боровой',
point: 0,
show: commonShow 
}
);

//добавляем баллы
students.filter(function(item){
	return (item.name == 'Ирина Овчинникова')||(item.name == 'Александр Малов')
}).forEach(function(item){
	item.point +=30
});

students.find(function(item){
	return item.name == 'Николай Фролов';
}).point +=10;

var changeStudent = students.find(function(item){
	return item.name == 'Дмитрий Фитискин';
});

if (changeStudent == undefined) {
	changeStudent =	{	name: 'Дмитрий Фитискин',
										point: 0,
										show: commonShow 
									};
	students.push(changeStudent);
};
changeStudent.point +=5;

//выводим студентов с баллами 30 и более
students.forEach(function(item){
	if (item.point >= 30) {
		item.show();
	}
}
);
//добавляем свойство количество работ
students.forEach(function(item){
	item.worksAmount = item.point/10;
}
);
//добавляем массиву метод
students.findByName = function(name){
	return this.find(function(item){
		return item.name == name;
	})
};

